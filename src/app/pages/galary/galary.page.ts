import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent, Platform} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Location } from "@angular/common";
@Component({
  selector: 'app-galary',
  templateUrl: './galary.page.html',
  styleUrls: ['./galary.page.scss'],
})
export class GalaryPage implements OnInit {

  constructor(private storage: Storage, 
    public platform: Platform, private location: Location){
      this.initializeApp();
  }

  canvas: HTMLCanvasElement;
  canvasCtx: CanvasRenderingContext2D;
  brushRadius: number;
  img = new Image();
  fullHeight: number;
  pages: number[];
  eyeIcon: string;
  scrollPosition: number;
  lineHeight: number;
  fabUp: HTMLElement;
  fabDown: HTMLElement;
  @ViewChild(IonContent) content: IonContent;

  initializeApp(){
    this.platform.backButton.subscribeWithPriority(0, () => {
      this.saveScrollPosition();
      this.location.back();
    });
  }

  ngOnInit() {
    this.pages = Array(3).fill(0).map((x,i)=>i);
    this.eyeIcon = "eye";
    this.canvas  = <HTMLCanvasElement>document.getElementById('js-canvas');
    this.fabUp = <HTMLElement>document.getElementById('fab-up');
    this.fabDown = <HTMLElement>document.getElementById('fab-down');
    this.canvasCtx = this.canvas.getContext('2d');
    this.canvas.style.height = (3*(window.innerHeight-56))+'px';
    this.canvas.style.width = (window.innerWidth-10)+'px';
    this.canvas.height = 3*(window.innerHeight-56);
    this.canvas.width = (window.innerWidth-10);
    this.brushRadius = (this.canvas.width / 100) * 5;
    this.scrollPosition = 0;
    this.lineHeight = (window.innerHeight-56)/6;
    if (this.brushRadius < 50) 
    { 
      this.brushRadius = 50;
    }
    
    this.drawRectanglesOnCanvas();
    (<HTMLElement>document.querySelector('.form')).style.visibility = 'visible';
    this.canvas.addEventListener('mousemove', this.handleMouseMove.bind(this), false);
    this.canvas.addEventListener('touchmove', this.handleTouchMove.bind(this), false);
    
    this.setInitialScrollPosition();
  }

  setInitialScrollPosition(){
    this.storage.get('scrollPosition').then((val) => {
      if(val){
        this.scrollPosition = val;
        this.content.scrollToPoint(0, val);
      }
    }).catch((error) => {
      console.log('get error for scrollPosition', error);
    });
  }

  drawRectanglesOnCanvas(){
    // this.img.src = './assets/images/scratchcard.png';
    // this.img.onload = () => {
    //   this.canvasCtx.drawImage(this.img, 0, 0, this.canvas.width, this.canvas.height);
    //   (<HTMLElement>document.querySelector('.form')).style.visibility = 'visible';
    // };
    //for(var i=0; i<this.canvas.height; i)
    var rectX = 15;
    var rectWidth = this.canvas.width - 20;
    var pageLineHeight = (window.innerHeight-56)/6;
    var translationHeight = pageLineHeight*0.37;
    var rectHeight = pageLineHeight - translationHeight; // removing urdu translation from line height
    var rectCountPerPage = 0;
    for(var rectY = 24; rectY < this.canvas.height; rectY =  (rectY + pageLineHeight)-10){
      this.canvasCtx.fillRect(rectX, rectY, rectWidth, rectHeight);
      rectCountPerPage++;
      if(rectCountPerPage == 6){
        rectY = rectY + 60;
        rectCountPerPage = 0;
      }
    }
  }

  detectLeftButton(event) {
    if ('buttons' in event) {
        return event.buttons === 1;
    } else if ('which' in event) {
        return event.which === 1;
    } else {
        return event.button === 1;
    }
  }

 getBrushPos(xRef, yRef) {
	var canvasRect = this.canvas.getBoundingClientRect();
    return {
	  x: Math.floor((xRef-canvasRect.left)/(canvasRect.right-canvasRect.left)*this.canvas.width),
	  y: Math.floor((yRef-canvasRect.top)/(canvasRect.bottom-canvasRect.top)*this.canvas.height)
    };
 }
      
 drawDot(mouseX,mouseY){
	this.canvasCtx.beginPath();
  this.canvasCtx.arc(mouseX, mouseY, this.brushRadius, 0, 2*Math.PI, true);
  this.canvasCtx.fillStyle = '#000';
  this.canvasCtx.globalCompositeOperation = "destination-out";
  this.canvasCtx.fill();
}

  handleMouseMove(e) {
    var brushPos = this.getBrushPos(e.clientX, e.clientY);
    var leftBut = this.detectLeftButton(e);
    if (leftBut) {
      this.drawDot(brushPos.x, brushPos.y);
    }
  }

  handleTouchMove(e) {
    e.preventDefault();
    var touch = e.targetTouches[0];
    if (touch) {
    var brushPos = this.getBrushPos(touch.pageX, touch.pageY);
        this.drawDot(brushPos.x, brushPos.y);
    }
  }

  toggleOverlay(){
    if(this.canvas.style.visibility == "hidden" || !this.canvas.style.visibility){
      this.canvas.style.visibility = "visible";
      this.eyeIcon = "eye-off";
      this.fabUp.style.visibility = 'visible';
      this.fabDown.style.visibility = 'visible';
    }
    else{
      this.canvas.style.visibility = "hidden";
      this.eyeIcon = "eye";
      this.fabUp.style.visibility = 'hidden';
      this.fabDown.style.visibility = 'hidden';
    }
    
  }

  scrollDown() {
    this.scrollPosition = this.scrollPosition + this.lineHeight;
    this.content.scrollToPoint(0, this.scrollPosition, 500);
    this.setValue("scrollPosition", this.scrollPosition);
  }

  scrollUp() {
    this.scrollPosition = this.scrollPosition - this.lineHeight;
    this.content.scrollToPoint(0, this.scrollPosition, 500);
    this.setValue("scrollPosition", this.scrollPosition);
  }
  saveScrollPosition(){
    this.content.getScrollElement().then((response) => {
      console.log(response.scrollTop);
      this.setValue("scrollPosition", response.scrollTop);
    }).catch((error) => {
    });
  }

  setValue(key: string, value: any) {
    this.storage.set(key, value).then((response) => {
      console.log('set' + key + ' ', response);
    }).catch((error) => {
      console.log('set error for ' + key + ' ', error);
    });
  }

}
